/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin17_1;

/**
 *
 * @author rsanromanlorenzo
 */
public class Metodos {
/**
 * Metodo que asigna valores a un array de enteros
 * @param arrayaasignar introduzco array de enteros como parametro
 */
    public static void asignaValores(int[] arrayaasignar) {
        for (int i = 0; i < arrayaasignar.length; i++) {
            arrayaasignar[i] = (int) (Math.random() * 50 + 1);
            //Math.random()*(Maximo-minimo)+minimo
        }

    }
/**
 * Metodo para visualizar valores de un array de enteros
 * @param array Introduzco array de enteros como parametro
 */
    public static void ver(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println("nº " + (i + 1) + " es " + array[i]);
        }

    }
  /**
   * Metodo para reordear valores dun array, reasignando suas posicións.
   * @param array Introduzco array de enteros como parametro
   */  
  public static void reordear(int[] array) {
        int j = array.length - 1;
        int aux;
        for (int i = 0; i < array.length / 2; i++) {
            aux = array[j];
            array[j] = array[i];
            array[i] = aux;

            j = j - 1;
        }
    }


}
