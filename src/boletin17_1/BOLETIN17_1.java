package boletin17_1;
/**
 * 
 * @author rsanromanlorenzo
 * @version version master
 */
public class BOLETIN17_1 {

    public static void main(String[] args) {
        int[] numeros = new int[6];

        Metodos.asignaValores(numeros);
        Metodos.ver(numeros);

        Metodos.reordear(numeros);
        Metodos.ver(numeros);

    }

}
